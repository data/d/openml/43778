# OpenML dataset: Top-10000-Movies-Based-On-Ratings

https://www.openml.org/d/43778

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
People love movies because:

  It takes you on a journey.
  Its an escape from reality.

Being a vivid movie watcher I always get amazed how sites like Netflix and Hotstar always exactly suggest the next movie I planned to watch on the back of mind. I researched a lot and decide to come up with something similar to that, so I decided to start with extracting a huge dataset of movies people love to watch and apply analysis on it.
Content
The dataset contains the following information:

Popularity: How popular the movie is.
Vote Count: Number of people voted.
Title: Name of the movie.
Vote Average: Average number of people voted to watch this movie.
Overview: Brief overview of what movie is (storyline).
Release Date: Date when the movie was released.

Inspiration
I would love to get the following answer:

Relationship between popularity and average vote count?
Which machine algorithm would be effective to find relationship between movies?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43778) of an [OpenML dataset](https://www.openml.org/d/43778). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43778/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43778/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43778/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

